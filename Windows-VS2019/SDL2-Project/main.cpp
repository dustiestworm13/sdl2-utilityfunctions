/*
  Starting to use functions to avoid repeating code and manage complexity. 
*/

//For exit()
#include <stdlib.h>

// For printf()
#include <stdio.h>

// For round()
#include <math.h>

#if defined(_WIN32) || defined(_WIN32)
    //The SDL library
    #include "SDL.h"
    //Support for loading different types of images.
    #include "SDL_image.h"
#else
    #include "SDL2/SDL.h"
    #include "SDL2/SDL_image.h"
#endif

/*****************************
 * Global Variables           *
 * ***************************/

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT= 600;

const int SDL_OK = 0;

/****************************
 * Function Prototypes      *
 * **************************/

SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer);

int main( int argc, char* args[] )
{
     // Declare window and renderer objects
    SDL_Window*	     gameWindow = nullptr;
    SDL_Renderer*    gameRenderer = nullptr;

    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     backgroundTexture = nullptr;
    SDL_Texture*     playerTexture = nullptr;

    // Player Image - source rectangle
    // - The part of the texture to render.
    // - Note: This is an SDL structure!!
    const int KING_SPRITE_HEIGHT = 64;
    const int KING_SPRITE_WIDTH = 32;
    SDL_Rect targetRectangle;
    float playerSpeed = 50.0f;
    float playerX = 250.0f;
    float playerY = 250.0f;

    // Window control 
    SDL_Event event;
    bool quit = false;  //false

    //Input - keys/joysticks?
    float verticalInput = 0.0f; 
    float horizontalInput = 0.0f; 
    
    // Keyboard
    const Uint8 *keyStates;
    
    // Timing variables
    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex; 
    unsigned int timeDelta;
    float timeDeltaInSeconds;

    //For Animation - animation frames
    const int MAX_ANIMATION_FRAMES = 3;
    SDL_Rect walkFrames[MAX_ANIMATION_FRAMES];

    int currentFrame;
    float accumulator;

    // SDL allows us to choose which SDL components are going to be
    // initialised. We'll go for everything for now!
    int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

    if(sdl_status != SDL_OK)
    {
        //SDL did not initialise, report and error and exit. 
        printf("Error -  SDL Initialisation Failed\n");
        exit(1);
    }

    gameWindow = SDL_CreateWindow("Hello CIS4008",   // Window title
                            SDL_WINDOWPOS_UNDEFINED, // X position
                            SDL_WINDOWPOS_UNDEFINED, // Y position
                            WINDOW_WIDTH,            // width
                            WINDOW_HEIGHT,           // height               
                            SDL_WINDOW_SHOWN);       // Window flags

   
    
    if(gameWindow != nullptr)
    {
        // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);

        if(gameRenderer == nullptr)
        {
          printf("Error - SDL could not create renderer\n");
          exit(1);
        }
    }
    else
    {
        // could not create the window, so don't try and create the renderer. 
        printf("Error - SDL could not create Window\n");
        exit(1);
    }
    
     // Track Keystates array
	keyStates = SDL_GetKeyboardState(NULL);
      
    /**********************************
     *    Setup background image     *
     * ********************************/

    // Create background texture from file, optimised for renderer 
    backgroundTexture = createTextureFromFile("assets/images/background.png", gameRenderer);


    /**********************************
     *    Setup undeadking image     *
     * ********************************/

    // Create player texture from file, optimised for renderer 
    playerTexture = createTextureFromFile("assets/images/undeadking.png", gameRenderer);

    //Setup animation frames
    for(int i = 0; i < MAX_ANIMATION_FRAMES; i++)
    {
        walkFrames[i].x = (i * KING_SPRITE_WIDTH); //ith col.
        walkFrames[i].y = (2* KING_SPRITE_HEIGHT); //3rd row. 
        walkFrames[i].w = KING_SPRITE_WIDTH;
        walkFrames[i].h = KING_SPRITE_HEIGHT;
    }


    //set current animation frame to first frame. 
    currentFrame = 0;

    //zero frame time accumulator
    accumulator = 0.0f;

    // Target is the same size of the source
    // We could choose any size - experiment with this :-D
    targetRectangle.w = KING_SPRITE_WIDTH;
    targetRectangle.h = KING_SPRITE_HEIGHT;


    // initialise preTimeIndex
    prevTimeIndex = SDL_GetTicks();


     // Game loop
    while(!quit) // while quit is not true
    { 
	    // Calculate time elapsed
        // Better approaches to this exist - https://gafferongames.com/post/fix_your_timestep/

        currentTimeIndex = SDL_GetTicks();	
        timeDelta = currentTimeIndex - prevTimeIndex; //time in milliseconds
        timeDeltaInSeconds = timeDelta * 0.001f;
        	
        // Store current time index into prevTimeIndex for next frame
        prevTimeIndex = currentTimeIndex;

        // Store animation time delta
        accumulator += timeDeltaInSeconds;

        // Handle input 

        if( SDL_PollEvent( &event ))  // test for events
        { 
            switch(event.type) 
            { 
                case SDL_QUIT:
    		        quit = true;
    		    break;

                // Key pressed event
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    }
                break;

                // Key released event
                case SDL_KEYUP:
                    switch (event.key.keysym.sym)
                    {
                    case SDLK_ESCAPE:
                        //  Nothing to do here.
                        break;
                    }
                break;
                
                default:
                    // not an error, there's lots we don't handle. 
                    break;    
            }
        }

        

         // This could be more complex, e.g. increasing the vertical 
         // input while the key is held down. 
        if (keyStates[SDL_SCANCODE_RIGHT]) 
        {
            horizontalInput = 1.0f;
        }
        else 
        {
            horizontalInput = 0.0f;
        }

         // Update Game World

	    // Calculate player velocity. 
	    // Note: This is imperfect, no account taken of diagonal!
        float xVelocity = verticalInput * playerSpeed;
        float yVelocity = horizontalInput * playerSpeed;

        // Calculate distance travelled since last update
        float yMovement = timeDeltaInSeconds * xVelocity;
        float xMovement = timeDeltaInSeconds * yVelocity;

        // Update player position.
	    playerX += xMovement;
        playerY += yMovement;

        // Move sprite to nearest pixel location.
	    targetRectangle.y = round(playerY);
	    targetRectangle.x = round(playerX);

        // Check if animation needs update
        if(accumulator > 0.40f) //25fps
        {
          currentFrame++;
          accumulator = 0.0f;

          if(currentFrame >= MAX_ANIMATION_FRAMES)
          {
            currentFrame = 0;
          }
          
        }

        //Draw stuff here.

        // 1. Clear the screen
        SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
        // Colour provided as red, green, blue and alpha (transparency) values (ie. RGBA)

        SDL_RenderClear(gameRenderer);

        // 2. Draw the scene
        SDL_RenderCopy(gameRenderer, backgroundTexture, NULL, NULL);

        SDL_RenderCopy(gameRenderer, playerTexture, &walkFrames[currentFrame], &targetRectangle);

        // 3. Present the current frame to the screen
        SDL_RenderPresent(gameRenderer);

    }
    
    //Clean up!
    SDL_DestroyTexture(playerTexture);
    playerTexture = nullptr;   
   
    SDL_DestroyTexture(backgroundTexture);
    backgroundTexture = nullptr;
    
    SDL_DestroyRenderer(gameRenderer);
    gameRenderer = nullptr;
    
    SDL_DestroyWindow(gameWindow);
    gameWindow = nullptr;   

    //Shutdown SDL - clear up resources etc.
    SDL_Quit();

    exit(0);
}


/**
 * Create Texture from File
 * 
 * Function to load a texture from a given filename and render context.
 * 
 * @param filename Filename of the file to load. 
 * @param renderer A pointer to the current SDL_Renderer. 
 * @return A pointer to the SDL_Texture created, or a nullptr on error. 
 */ 
SDL_Texture* createTextureFromFile(const char* filename, SDL_Renderer *renderer)
{
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture*     texture = nullptr;

    // Temporary surface used while loading the image
    SDL_Surface*     temp = nullptr;

    // Load the sprite to our temp surface
    temp = IMG_Load(filename);

    if(temp == nullptr)
    {
        printf("%s image not found!", filename);
    }    
    else  // Improved error handling, if we have no surface we can't make a texture.
    {
        // Create a texture object from the loaded image
        // - we need the renderer we're going to use to draw this as well!
        // - this provides information about the target format to aid optimisation.
        texture = SDL_CreateTextureFromSurface(renderer, temp);

        // Clean-up - we're done with 'image' now our texture has been created
        SDL_FreeSurface(temp);
        temp = nullptr;
    }

    return texture;
}